// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"

#include "TDS_Unreal/Interface/TDS_UnrealIGameActor.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, FName HitBoneName, TSubclassOf<UTDS_UnrealStateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if(SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		UTDS_UnrealStateEffect* myEffect = Cast<UTDS_UnrealStateEffect>(AddEffectClass->GetDefaultObject());

		if(myEffect)
		{
			bool bIsHavePossibleSurface = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsHavePossibleSurface)
			{
				if(myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bIsHavePossibleSurface = true;
					bool bIsCanAddEffect = false;

					if(!myEffect->bIsStackable)
					{
						ITDS_UnrealIGameActor* MyInterface = Cast<ITDS_UnrealIGameActor>(TakeEffectActor);

						if(MyInterface)
						{
							TArray<UTDS_UnrealStateEffect*> MyEffects = MyInterface->GetAllCurrentEffects();
							int8 j = 0;

							if(MyEffects.Num() > 0)
							{
								while (j < MyEffects.Num() && !bIsCanAddEffect)
								{
									if (MyEffects[j]->GetClass() != AddEffectClass)
									{
										bIsCanAddEffect = true;
									}
									j++;
								}		
							} else
							{
								bIsCanAddEffect = true;
							}
						}
					} else
					{
						bIsCanAddEffect = true;
					}

					if (bIsCanAddEffect)
					{
						UTDS_UnrealStateEffect* NewEffect = NewObject<UTDS_UnrealStateEffect>(TakeEffectActor, AddEffectClass);

						if(NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor, HitBoneName);
						}
					}
				}
				i++;
			}
		}		
	}
}
