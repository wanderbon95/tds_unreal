// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_UnrealCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "TDS_Unreal/Game/TDS_UnrealGameInstance.h"
#include "Math/UnrealMathUtility.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

ATDS_UnrealCharacter::ATDS_UnrealCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = true; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UTDS_UnrealInventorySystem>(TEXT("InventoryComponent"));
	HealthComponent = CreateDefaultSubobject<UTDS_UnrealCharHealthComponent>(TEXT("HealthComponent"));
	
	if (InventoryComponent) {
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDS_UnrealCharacter::InitWeapon);
	}

	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &ATDS_UnrealCharacter::Dead);
	}

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
}

void ATDS_UnrealCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
}

void ATDS_UnrealCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}	
}

void ATDS_UnrealCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDS_UnrealCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDS_UnrealCharacter::InputAxisY);
	NewInputComponent->BindAxis(TEXT("MouseWheel"), this, &ATDS_UnrealCharacter::InputAxisMouseWheel);
    
    NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDS_UnrealCharacter::InputAttackPressed);
    NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDS_UnrealCharacter::InputAttackReleased);
    
    NewInputComponent->BindAction(TEXT("SprintEvent"), EInputEvent::IE_Pressed,
        this, &ATDS_UnrealCharacter::InputSprintPressed);
    NewInputComponent->BindAction(TEXT("SprintEvent"), EInputEvent::IE_Released, this, &ATDS_UnrealCharacter::InputSprintReleased);
    
    NewInputComponent->BindAction(TEXT("WalkEvent"), EInputEvent::IE_Pressed,
        this, &ATDS_UnrealCharacter::InputWalkPressed);
    NewInputComponent->BindAction(TEXT("WalkEvent"), EInputEvent::IE_Released, this, &ATDS_UnrealCharacter::InputWalkReleased);
    
    NewInputComponent->BindAction(TEXT("AimEvent"), EInputEvent::IE_Pressed,
        this, &ATDS_UnrealCharacter::InputAimPressed);
    NewInputComponent->BindAction(TEXT("AimEvent"), EInputEvent::IE_Released, this, &ATDS_UnrealCharacter::InputAimReleased);
	
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATDS_UnrealCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATDS_UnrealCharacter::TrySwitchNextWeapon);
	NewInputComponent->BindAction(TEXT("SwitchPrevWeapon"), EInputEvent::IE_Pressed, this, &ATDS_UnrealCharacter::TrySwitchPreviousWeapon);

	NewInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &ATDS_UnrealCharacter::TryAddAbility);
	NewInputComponent->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Pressed, this, &ATDS_UnrealCharacter::DropCurrentWeapon);

	TArray<FKey> HotKeys;

	HotKeys.Add(EKeys::Zero);
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);

	NewInputComponent->BindKey(HotKeys[0], EInputEvent::IE_Pressed, this, &ATDS_UnrealCharacter::TKeyPressed<0>);
	NewInputComponent->BindKey(HotKeys[1], EInputEvent::IE_Pressed, this, &ATDS_UnrealCharacter::TKeyPressed<1>);
	NewInputComponent->BindKey(HotKeys[2], EInputEvent::IE_Pressed, this, &ATDS_UnrealCharacter::TKeyPressed<2>);
	NewInputComponent->BindKey(HotKeys[3], EInputEvent::IE_Pressed, this, &ATDS_UnrealCharacter::TKeyPressed<3>);
	NewInputComponent->BindKey(HotKeys[4], EInputEvent::IE_Pressed, this, &ATDS_UnrealCharacter::TKeyPressed<4>);
	NewInputComponent->BindKey(HotKeys[5], EInputEvent::IE_Pressed, this, &ATDS_UnrealCharacter::TKeyPressed<5>);
	NewInputComponent->BindKey(HotKeys[6], EInputEvent::IE_Pressed, this, &ATDS_UnrealCharacter::TKeyPressed<6>);
	NewInputComponent->BindKey(HotKeys[7], EInputEvent::IE_Pressed, this, &ATDS_UnrealCharacter::TKeyPressed<7>);
	NewInputComponent->BindKey(HotKeys[8], EInputEvent::IE_Pressed, this, &ATDS_UnrealCharacter::TKeyPressed<8>);
	NewInputComponent->BindKey(HotKeys[9], EInputEvent::IE_Pressed, this, &ATDS_UnrealCharacter::TKeyPressed<9>);
}

void ATDS_UnrealCharacter::InputAttackPressed()
{
    if (bIsAlive)
    {
    	AttackCharEvent(true);
    } else
    {
	    AttackCharEvent(false);
    }
}

void ATDS_UnrealCharacter::InputAttackReleased()
{
    AttackCharEvent(false);
}

void ATDS_UnrealCharacter::InputSprintPressed()
{
    SprintEnabled = true;
    ChangeMovementState();
}

void ATDS_UnrealCharacter::InputSprintReleased()
{
    SprintEnabled = false;
    ChangeMovementState();
}

void ATDS_UnrealCharacter::InputWalkPressed()
{
    WalkEnabled = true;
    ChangeMovementState();
}

void ATDS_UnrealCharacter::InputWalkReleased()
{
    WalkEnabled = false;
    ChangeMovementState();
}

void ATDS_UnrealCharacter::InputAimPressed()
{
    AimEnabled = true;
    ChangeMovementState();
}

void ATDS_UnrealCharacter::InputAimReleased()
{
    AimEnabled = false;
    ChangeMovementState();
}

void ATDS_UnrealCharacter::InputAxisX(float Value)
{
    AxisX = Value;
}

void ATDS_UnrealCharacter::InputAxisY(float Value)
{
    AxisY = Value;
}

void ATDS_UnrealCharacter::InputAxisMouseWheel(float Value)
{
	if (Value != 0 && ((SlideDone && SlideWithSmooth) || !SlideWithSmooth)) {
		float ArmLength = CameraBoom->TargetArmLength;

		if (Value < 0) {
			float NewCameraHeight = HeightCameraChangeDistance + ArmLength;

			if (HeightCameraMax >= NewCameraHeight) {
				SlideUp = true;
				SlideDown = false;
				SlideDone = false;

				if (SlideWithSmooth) {
					ATDS_UnrealCharacter::RunTimer();
				}
				else {
					CameraBoom->TargetArmLength = NewCameraHeight;
				}
			}
		}
		else {
			float NewCameraHeight = ArmLength - HeightCameraChangeDistance;

			if (HeightCameraMin <= NewCameraHeight) {
				SlideDown = true;
				SlideUp = false;
				SlideDone = false;

				if (SlideWithSmooth) {
					ATDS_UnrealCharacter::RunTimer();
				}
				else {
					CameraBoom->TargetArmLength = NewCameraHeight;
				}
			}
		}
	}
}

void ATDS_UnrealCharacter::RunTimer()
{
	GetWorldTimerManager().SetTimer(TimerHandle, this, &ATDS_UnrealCharacter::RepeatSlide, TimerStep, true);
}

void ATDS_UnrealCharacter::RepeatSlide()
{
	CurrentSlideDistance += SlideDistanceStep;
	float NewTargetArmLength = CameraBoom->TargetArmLength;

	if (SlideUp) {
		NewTargetArmLength += SlideDistanceStep;
	}
	else {
		NewTargetArmLength -= SlideDistanceStep;
	}

	CameraBoom->TargetArmLength = NewTargetArmLength;

	if (CurrentSlideDistance >= HeightCameraChangeDistance) {
		CurrentSlideDistance = 0;
		SlideDone = true;

		ATDS_UnrealCharacter::ClearTimer();
	}
}

void ATDS_UnrealCharacter::ClearTimer()
{
	if (GetWorldTimerManager().TimerExists(TimerHandle)) {
		GetWorldTimerManager().ClearTimer(TimerHandle);
	}
}


void ATDS_UnrealCharacter::MovementTick(float DeltaTime)
{
	if(!bIsAlive)
	{
		return;
	}
	
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	if (MovementState == EMovementState::SprintRun_State)
	{
		FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
		FRotator myRotator = myRotationVector.ToOrientationRotator();
		SetActorRotation((FQuat(myRotator)));
	}
	else
	{
		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController)
		{
			FHitResult ResultHit;
			//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);// bug was here Config\DefaultEngine.Ini
			myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

			float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));

			if (CurrentWeapon)
			{
				FVector Displacement = FVector(0);
				switch (MovementState)
				{
				case EMovementState::Aim_State:
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::AimWalk_State:
					CurrentWeapon->ShouldReduceDispersion = true;
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					break;
				case EMovementState::Walk_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Run_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::SprintRun_State:
					break;
				default:
					break;
				}

				CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
				//aim cursor like 3d Widget?
			}

			CheckMoveIsForward();
		}
	}
}

void ATDS_UnrealCharacter::AttackCharEvent(bool bIsFiring)
{
    AWeaponDefault* myWeapon = nullptr;
    myWeapon = GetCurrentWeapon();
    if (myWeapon)
    {
        myWeapon->SetWeaponStateFire(bIsFiring);
    }
    else
        UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

void ATDS_UnrealCharacter::CharacterUpdate()
{
	float ResSpeed = MovementInfo.RunSpeedNormal;
	switch (MovementState)
	{
		case EMovementState::AimWalk_State:
			ResSpeed = MovementInfo.AimSpeedWalk;
			break;
		case EMovementState::Walk_State:
			ResSpeed = MovementInfo.WalkSpeedNormal;
			break;
		case EMovementState::Aim_State:
			ResSpeed = MovementInfo.AimSpeedNormal;
			break;
		case EMovementState::Run_State:
			ResSpeed = MovementInfo.RunSpeedNormal;
			break;
		case EMovementState::SprintRun_State:
			ResSpeed = MovementInfo.SprintRunSpeed ;
			break;
		default:
			break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDS_UnrealCharacter::ChangeMovementState()
{
	if ((!WalkEnabled && !AimEnabled && !SprintEnabled) || !MoveIsForward) {
		MovementState = EMovementState::Run_State;
	}
	else {
		if (SprintEnabled && MoveIsForward) {
			WalkEnabled = false;
			AimEnabled = false;
			MovementState = EMovementState::SprintRun_State;
		}

		if (WalkEnabled && AimEnabled && !SprintEnabled) {
			MovementState = EMovementState::AimWalk_State;
		}
		else {
			if (WalkEnabled && !AimEnabled && !SprintEnabled) {
				MovementState = EMovementState::Walk_State;
			}
			else {
				if (AimEnabled && !WalkEnabled && !SprintEnabled) {
					MovementState = EMovementState::Aim_State;
				}
			}
		}
	}

	CharacterUpdate();

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if(myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

void ATDS_UnrealCharacter::CheckMoveIsForward() {
	bool PrevValue = MoveIsForward;

	if (Direction >= -5 && Direction <= 5) {
		MoveIsForward = true;

		if (!PrevValue) {
			ChangeMovementState();
		}
	}
	else {
		MoveIsForward = false;

		if (PrevValue) {
			ChangeMovementState();
		}		
	}
}

AWeaponDefault* ATDS_UnrealCharacter::GetCurrentWeapon()
{
    return CurrentWeapon;
}

int32 ATDS_UnrealCharacter::GetCurrentWeaponIndex()
{
	return CurrentIndexWeapon;
}

void ATDS_UnrealCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewIndexWeapon)
{
	if (CurrentWeapon) {
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UTDS_UnrealGameInstance* myGI = Cast<UTDS_UnrealGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					//Remove !!! Debug
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
					CurrentIndexWeapon = NewIndexWeapon;


					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATDS_UnrealCharacter::WeaponFireStart);
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDS_UnrealCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDS_UnrealCharacter::WeaponReloadEnd);
				
					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload()) {
						CurrentWeapon->InitReload();
					}

					if (InventoryComponent) {
						InventoryComponent->OnWeaponAmmoAvailable.Broadcast(myWeapon->WeaponSetting.WeaponType);
					}
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

void ATDS_UnrealCharacter::TryReloadWeapon()
{
	if (bIsAlive && CurrentWeapon && !CurrentWeapon->WeaponReloading && CurrentWeapon->CheckCanWeaponReload())
	{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound)
			CurrentWeapon->InitReload();
	}
}

void ATDS_UnrealCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATDS_UnrealCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe)
{
	if (InventoryComponent && CurrentWeapon) {
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoSafe);
		InventoryComponent->SetAdditionalWeaponInfo(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}

	WeaponReloadEnd_BP(bIsSuccess);
}

void ATDS_UnrealCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void ATDS_UnrealCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	// in BP
}

void ATDS_UnrealCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon) {
		InventoryComponent->SetAdditionalWeaponInfo(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}

	WeaponFireStart_BP(Anim);
}

void ATDS_UnrealCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

UDecalComponent* ATDS_UnrealCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ATDS_UnrealCharacter::TrySwitchNextWeapon()
{
	TrySwitchWeapon(1);
}

void ATDS_UnrealCharacter::TrySwitchPreviousWeapon()
{
	TrySwitchWeapon(-1);
}

void ATDS_UnrealCharacter::TrySwitchWeapon(int8 SwitchDirection)
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;

		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + SwitchDirection, OldIndex, OldInfo, SwitchDirection > 0))
			{
			}
		}
	}
}

void ATDS_UnrealCharacter::TryAddAbility()
{
	if (Ability)
	{
		bIsAbility = true;
		UTDS_UnrealStateEffect* NewEffect = NewObject<UTDS_UnrealStateEffect>(this, Ability);

		if (NewEffect)
		{
			NewEffect->InitObject(this);
		} else
		{
			bIsAbility = false;
		}
	}
}

void ATDS_UnrealCharacter::DropCurrentWeapon()
{
	if (InventoryComponent)
	{
		FDropItem DropItemInfo;
		InventoryComponent->DropWeaponByIndex(CurrentIndexWeapon, DropItemInfo);
	}
}

bool ATDS_UnrealCharacter::TrySwitchWeaponToIndexByKey(int32 ToIndex)
{
	bool bIsSuccess = false;
	
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent && InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
	{
		if (CurrentIndexWeapon != ToIndex)
		{
			int32 OldIndex = CurrentIndexWeapon;
			FAdditionalWeaponInfo OldAdditionalWeaponInfo;

			if (CurrentWeapon)
			{
				OldAdditionalWeaponInfo = CurrentWeapon->AdditionalWeaponInfo;

				if (CurrentWeapon->WeaponReloading)
				{
					CurrentWeapon->CancelReload();
				}

				bIsSuccess = InventoryComponent->SwitchWeaponToIndex(ToIndex, OldIndex, OldAdditionalWeaponInfo);
			}
		}
	}

	return bIsSuccess;
}

float ATDS_UnrealCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent,
                                       AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (bIsAlive)
	{
		HealthComponent->ChangeHealthValue(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);

		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, NAME_None, myProjectile->ProjectileSetting.Effect, GetSurfaceType());
		}
	}

	return ActualDamage;
}

EPhysicalSurface ATDS_UnrealCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;

	if (HealthComponent)
	{
		if (HealthComponent->GetCurrentShield() > 0.0f || bIsAbility)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);

				if (myMaterial)
				{
					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}		
		}
	}

	return Result;
}

TArray<UTDS_UnrealStateEffect*> ATDS_UnrealCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ATDS_UnrealCharacter::RemoveEffect(UTDS_UnrealStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATDS_UnrealCharacter::AddEffect(UTDS_UnrealStateEffect* AddEffect)
{
	Effects.Add(AddEffect);
}

FVector ATDS_UnrealCharacter::GetEffectSettings(FName& BoneName)
{
	BoneName = BoneNameForEffect;
	
	return EffectOffset;	
}

void ATDS_UnrealCharacter::CharDead_BP_Implementation()
{
}

void ATDS_UnrealCharacter::Dead()
{
	float AnimationLength = 2.0f;
	int32 MontageIndex = FMath::RandHelper(DeadsAnim.Num());

	if (DeadsAnim.IsValidIndex(MontageIndex) && DeadsAnim[MontageIndex] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		AnimationLength = DeadsAnim[MontageIndex]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[MontageIndex]);
	}

	bIsAlive = false;

	AttackCharEvent(false);

	if (GetController())
	{
		GetController()->UnPossess();
	}

	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATDS_UnrealCharacter::EnableRagdoll, AnimationLength, false);
	GetCursorToWorld()->SetVisibility(false);

	CharDead_BP();
}

void ATDS_UnrealCharacter::EnableRagdoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}
