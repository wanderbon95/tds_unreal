// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS_Unreal/FuncLibrary/Types.h"
#include "TDS_Unreal/Weapons/WeaponDefault.h"
#include "TDS_UnrealInventorySystem.h"
#include "TDS_Unreal/Character/TDS_UnrealCharHealthComponent.h"
#include "TDS_Unreal/Interface/TDS_UnrealIGameActor.h"

#include "TDS_UnrealCharacter.generated.h"

UCLASS(Blueprintable)
class ATDS_UnrealCharacter : public ACharacter, public ITDS_UnrealIGameActor
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	ATDS_UnrealCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	// FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	//Inventory
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTDS_UnrealInventorySystem* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTDS_UnrealCharHealthComponent* HealthComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	// UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	// class UDecalComponent* CursorToWorld;

	APlayerController* MyController;

public:
	FTimerHandle TimerHandle_RagDollTimer;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		bool bIsAlive = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		TArray<UAnimMontage*> DeadsAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UTDS_UnrealStateEffect> Ability;
	
	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Slide")
		bool SlideWithSmooth = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Slide")
		bool SlideDone = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Slide")
		bool SlideUp = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Slide")
		bool SlideDown = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Slide")
		float CurrentSlideDistance = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Slide")
		float SlideDistanceStep = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float HeightCameraMax = 1000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float HeightCameraMin = 500.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float HeightCameraChangeDistance = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Timer")
		FTimerHandle TimerHandle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Timer")
		float TimerStep = 0.001f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Slide")
		bool slideToCursor = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Slide")
		float fSpeedChangeLocationCameraToCursor = 8.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Slide")
		float fMinClampDistanceLogicMouseCursorStart = 500.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Slide")
		float fMaxClampDistanceLogicMouseCursorStart = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Slide")
		float fSpeedReturnCameraDefaltPosition = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Slide")
		float clampedCameraHeight_L = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Slide")
		float distanceCameraBoomToChar_L = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
		float Direction = 0.0f;

	bool MoveIsForward = false;

	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UFUNCTION()
		void InputAxisMouseWheel(float Value);

	void RunTimer();
	void RepeatSlide();
	void ClearTimer();

	//Weapon	
	AWeaponDefault* CurrentWeapon = nullptr;

	//for demo 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;

	UDecalComponent* CurrentCursor = nullptr;

	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
    
    UFUNCTION()
        void InputAttackPressed();
    UFUNCTION()
        void InputAttackReleased();
    UFUNCTION(BlueprintCallable)
        void AttackCharEvent(bool bIsFiring);
    
    // Movement state
    // Srpint state
    UFUNCTION()
        void InputSprintPressed();
    UFUNCTION()
        void InputSprintReleased();
    // Walk state
    UFUNCTION()
        void InputWalkPressed();
    UFUNCTION()
        void InputWalkReleased();
    // Aim state
    UFUNCTION()
        void InputAimPressed();
    UFUNCTION()
        void InputAimReleased();

	UFUNCTION()
		void MovementTick(float DeltaTime);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();
	UFUNCTION()
		void CheckMoveIsForward();
    
    UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();
    UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 GetCurrentWeaponIndex();
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewIndexWeapon);
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);
    
	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	//Inventory Func

	void TrySwitchNextWeapon();
	void TrySwitchPreviousWeapon();
	void TrySwitchWeapon(int8 SwitchDirection);
	void TryAddAbility();
	void DropCurrentWeapon();

	bool TrySwitchWeaponToIndexByKey(int32 ToIndex);

	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKey(Id);
	}

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;

	UFUNCTION()
		void Dead();
	
	void EnableRagdoll();
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	// Start Interface
	virtual EPhysicalSurface GetSurfaceType() override;
	virtual TArray<UTDS_UnrealStateEffect*> GetAllCurrentEffects() override;
	// End Interface

	//Effects
	TArray<UTDS_UnrealStateEffect*> Effects;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect Spawn Point Setting")
		FName BoneNameForEffect;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect Spawn Point Setting")
		FVector EffectOffset = FVector(0);
	
	virtual void RemoveEffect(UTDS_UnrealStateEffect* RemoveEffect) override;
	virtual void AddEffect(UTDS_UnrealStateEffect* AddEffect) override;
	virtual FVector GetEffectSettings(FName& BoneName) override;

	bool bIsAbility = false;
	
	UFUNCTION(BlueprintNativeEvent)
		void CharDead_BP();
};