// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_Unreal/Character/TDS_UnrealHealthComponent.h"

// Sets default values
UTDS_UnrealHealthComponent::UTDS_UnrealHealthComponent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryComponentTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void UTDS_UnrealHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void UTDS_UnrealHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

float UTDS_UnrealHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UTDS_UnrealHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UTDS_UnrealHealthComponent::ChangeHealthValue(float ChangeValue)
{
	
	ChangeValue = ChangeValue * CoefDamage;
	Health += ChangeValue;
	
	if (Health > 100.0f)
	{
		Health = 100.0f;
		OnHealthChange.Broadcast(Health, ChangeValue);
	} else {
		if (Health <= 0.0f)
		{
			Health = 0.0f;
			OnHealthChange.Broadcast(Health, ChangeValue);
			OnDead.Broadcast();	
		} else
		{
			OnHealthChange.Broadcast(Health, ChangeValue);
		}	
	}
}
