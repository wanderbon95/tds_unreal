// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_Unreal/Character/TDS_UnrealInventorySystem.h"
#include "TDS_Unreal/Game/TDS_UnrealGameInstance.h"
#include "TDS_Unreal/Interface/TDS_UnrealIGameActor.h"

// Sets default values for this component's properties
UTDS_UnrealInventorySystem::UTDS_UnrealInventorySystem()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTDS_UnrealInventorySystem::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UTDS_UnrealInventorySystem::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UTDS_UnrealInventorySystem::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;
	if (ChangeToIndex > WeaponSlots.Num()-1)
		CorrectIndex = 0;	
	else
		if(ChangeToIndex < 0)
			CorrectIndex = WeaponSlots.Num()-1;

	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;
	int32 NewCurrentIndex = 0;

	if (WeaponSlots.IsValidIndex(CorrectIndex))
	{
		if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
		{
			if (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0)
			{
				//good weapon have ammo start change
				bIsSuccess = true;
			}
			else
			{
				UTDS_UnrealGameInstance* myGI = Cast<UTDS_UnrealGameInstance>(GetWorld()->GetGameInstance());
				if (myGI)
				{
					//check ammoSlots for this weapon
					FWeaponInfo myInfo;
					myGI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, myInfo);

					bool bIsFind = false;
					int8 j = 0;
					while (j < AmmoSlots.Num() && !bIsFind)
					{
						if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
						{
							//good weapon have ammo start change
							bIsSuccess = true;
							bIsFind = true;
						}
						j++;
					}
				}
			}
			if (bIsSuccess)
			{
				NewCurrentIndex = CorrectIndex;
				NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
				NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
			}
		}
	}
	if (!bIsSuccess)
	{		
		int8 iteration = 0;
		int8 Seconditeration = 0;
		int8 tmpIndex = 0;
		while (iteration < WeaponSlots.Num() && !bIsSuccess)
		{
			iteration++;

			if (bIsForward)
			{
				//Seconditeration = 0;

				tmpIndex = ChangeToIndex + iteration;
			}
			else
			{
				Seconditeration = WeaponSlots.Num() - 1;

				tmpIndex = ChangeToIndex - iteration;
			}

			if (WeaponSlots.IsValidIndex(tmpIndex))
			{
				if (!WeaponSlots[tmpIndex].NameItem.IsNone())
				{
					if (WeaponSlots[tmpIndex].AdditionalInfo.Round > 0)
					{
						//WeaponGood
						bIsSuccess = true;
						NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
						NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
						NewCurrentIndex = tmpIndex;
					}
					else
					{
						FWeaponInfo myInfo;
						UTDS_UnrealGameInstance* myGI = Cast<UTDS_UnrealGameInstance>(GetWorld()->GetGameInstance());

						myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

						bool bIsFind = false;
						int8 j = 0;
						while (j < AmmoSlots.Num() && !bIsFind)
						{
							if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
							{
								//WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
								NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
								NewCurrentIndex = tmpIndex;
								bIsFind = true;
							}
							j++;
						}
					}
				}
			}
			else
			{
				//go to end of LEFT of array weapon slots
				if (OldIndex != Seconditeration)
				{
					if (WeaponSlots.IsValidIndex(Seconditeration))
					{
						if (!WeaponSlots[Seconditeration].NameItem.IsNone())
						{
							if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
							{
								//WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
								NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
								NewCurrentIndex = Seconditeration;
							}
							else
							{
								FWeaponInfo myInfo;
								UTDS_UnrealGameInstance* myGI = Cast<UTDS_UnrealGameInstance>(GetWorld()->GetGameInstance());

								myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlots.Num() && !bIsFind)
								{
									if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
									{
										//WeaponGood
										bIsSuccess = true;
										NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
										NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
										NewCurrentIndex = Seconditeration;
										bIsFind = true;
									}
									j++;
								}
							}
						}
					}
				}
				else
				{
					//go to same weapon when start
					if (WeaponSlots.IsValidIndex(Seconditeration))
					{
						if (!WeaponSlots[Seconditeration].NameItem.IsNone())
						{
							if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
							{
								//WeaponGood, it same weapon do nothing
							}
							else
							{
								FWeaponInfo myInfo;
								UTDS_UnrealGameInstance* myGI = Cast<UTDS_UnrealGameInstance>(GetWorld()->GetGameInstance());

								myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlots.Num() && !bIsFind)
								{
									if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
									{
										if (AmmoSlots[j].Cout > 0)
										{
											//WeaponGood, it same weapon do nothing
										}
										else
										{
											//Not find weapon with ammo need init Pistol with infinity ammo
											UE_LOG(LogTemp, Error, TEXT("UTPSInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
										}
									}
									j++;
								}
							}
						}
					}
				}
				if (bIsForward)
				{
					Seconditeration++;
				}
				else
				{
					Seconditeration--;
				}
				
			}
		}
	}	
	if (bIsSuccess)
	{
		SetAdditionalWeaponInfo(OldIndex, OldInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
	}		

	return bIsSuccess;
}

bool UTDS_UnrealInventorySystem::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex,
	FAdditionalWeaponInfo OldWeaponInfo)
{
	bool bIsSuccess = false;
	FName ToSwitchIdweapon;
	FAdditionalWeaponInfo ToSwitchAdditionalInfo;

	ToSwitchIdweapon = GetWeaponNameBySlotIndex(ChangeToIndex);
	ToSwitchAdditionalInfo = GetAdditionalWeaponInfo(ChangeToIndex);

	if (!ToSwitchIdweapon.IsNone())
	{
		SetAdditionalWeaponInfo(OldIndex, OldWeaponInfo);
		OnSwitchWeapon.Broadcast(ToSwitchIdweapon, ToSwitchAdditionalInfo, ChangeToIndex);

		EWeaponType ToSwitchWeaponType;

		if (GetWeaponTypeByNameWeapon(ToSwitchIdweapon, ToSwitchWeaponType))
		{
			int32 AvailableAmmo = -1;

			if (CheckAmmoForWeapon(ToSwitchWeaponType, AvailableAmmo))
			{
				
			}
		}

		bIsSuccess = true;
	}
	
	return bIsSuccess;
}

FAdditionalWeaponInfo UTDS_UnrealInventorySystem::GetAdditionalWeaponInfo(int32 IndexWeapon)
{
	FAdditionalWeaponInfo result;

	if (WeaponSlots.IsValidIndex(IndexWeapon)) {
		bool bIsFind = false;
		int8 i = 0;

		while (i < WeaponSlots.Num() && !bIsFind) {
			if (i == IndexWeapon) {
				result = WeaponSlots[i].AdditionalInfo;
				bIsFind = true;
			}

			i++;
		}

		if (!bIsFind) {
			UE_LOG(LogTemp, Warning, TEXT("UTDS_UnrealInventorySystem::GetAdditionalWeaponInfo - No Found Weapon with index - %d"), IndexWeapon);
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("UTDS_UnrealInventorySystem::GetAdditionalWeaponInfo - Not Correct index Weapon - %d"), IndexWeapon);
	}

	return result;
}

int32 UTDS_UnrealInventorySystem::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFind = false;

	while (i < WeaponSlots.Num() && !bIsFind) {
		if (WeaponSlots[i].NameItem == IdWeaponName) {
			result = i;
			bIsFind = true;
		}

		i++;
	}

	return result;
}

FName UTDS_UnrealInventorySystem::GetWeaponNameBySlotIndex(int32 IndexSlot)
{
	FName result;

	if (WeaponSlots.IsValidIndex(IndexSlot)) {
		result = WeaponSlots[IndexSlot].NameItem;
	}

	return result;
}

bool UTDS_UnrealInventorySystem::GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType& WeaponType)
{
	bool bIsFind = false;
	FWeaponInfo OutInfo;
	WeaponType = EWeaponType::RifleType;
	UTDS_UnrealGameInstance* myGI = Cast<UTDS_UnrealGameInstance>(GetWorld()->GetGameInstance());

	if (myGI)
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			myGI->GetWeaponInfoByName(WeaponSlots[IndexSlot].NameItem, OutInfo);
			WeaponType = OutInfo.WeaponType;
			bIsFind = true;	
		}
	}
	
	return bIsFind;
}

bool UTDS_UnrealInventorySystem::GetWeaponTypeByNameWeapon(FName WeaponIdName, EWeaponType& WeaponType)
{
	bool bIsFind = false;
	FWeaponInfo OutInfo;
	WeaponType = EWeaponType::RifleType;
	UTDS_UnrealGameInstance* myGI = Cast<UTDS_UnrealGameInstance>(GetWorld()->GetGameInstance());

	if (myGI)
	{
		myGI->GetWeaponInfoByName(WeaponIdName, OutInfo);
		WeaponType = OutInfo.WeaponType;
		bIsFind = true;
	}
	
	return bIsFind;
}

void UTDS_UnrealInventorySystem::SetAdditionalWeaponInfo(int32 IndexWeapon, FAdditionalWeaponInfo NewWeaponInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon)) {
		bool bIsFind = false;
		int8 i = 0;

		while (i < WeaponSlots.Num() && !bIsFind) {
			if (i == IndexWeapon) {
				WeaponSlots[i].AdditionalInfo = NewWeaponInfo;
				bIsFind = true;

				OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewWeaponInfo);
			}

			i++;
		}

		if (!bIsFind) {
			UE_LOG(LogTemp, Warning, TEXT("UTDS_UnrealInventorySystem::SetAdditionalWeaponInfo - No Found Weapon with index - %d"), IndexWeapon);
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("UTDS_UnrealInventorySystem::SetAdditionalWeaponInfo - Not Correct index Weapon - %d"), IndexWeapon);
	}
}

void UTDS_UnrealInventorySystem::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo)
{
	bool bIsFind = false;
	int8 i = 0;

	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon) {
			AmmoSlots[i].Cout += CoutChangeAmmo;

			if (AmmoSlots[i].Cout > AmmoSlots[i].MaxCout) {
				AmmoSlots[i].Cout = AmmoSlots[i].MaxCout;
			}

			OnAmmoChange.Broadcast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);
			
			bIsFind = true;
		}

		i++;
	}
}

bool UTDS_UnrealInventorySystem::CheckAmmoForWeapon(EWeaponType TypeWeapon, int32 &AvailableAmmoForWeapon)
{
	AvailableAmmoForWeapon = 0;
	bool bIsFind = false;
	int8 i = 0;

	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon) {
			bIsFind = true;
			AvailableAmmoForWeapon = AmmoSlots[i].Cout;

			if (AmmoSlots[i].Cout > 0) {
				return true;
			}
		}

		i++;
	}

	if (AvailableAmmoForWeapon <= 0)
	{
		OnWeaponAmmoEmpty.Broadcast(TypeWeapon);
	}
	else
	{
		OnWeaponAmmoAvailable.Broadcast(TypeWeapon);
	}

	return false;
}

bool UTDS_UnrealInventorySystem::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool result = false;
	int8 i = 0;

	while (i < AmmoSlots.Num() && !result)
	{
		if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Cout < AmmoSlots[i].MaxCout) {
			result = true;
		}

		i++;
	}

	return result;
}

bool UTDS_UnrealInventorySystem::CheckCanTakeWeapon(int32 &FreeSlot)
{
	bool bIsFreeSlot = false;
	int8 i = 0;

	while (i < WeaponSlots.Num() && !bIsFreeSlot)
	{
		if (WeaponSlots[i].NameItem.IsNone()) {
			bIsFreeSlot = true;
			FreeSlot = i;
		}

		i++;
	}

	return bIsFreeSlot;
}

bool UTDS_UnrealInventorySystem::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeapon, FDropItem& DropItemInfo)
{
	bool result = false;

	if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo)) {
		WeaponSlots[IndexSlot] = NewWeapon;

		SwitchWeaponToIndex(CurrentIndexWeapon, -1, NewWeapon.AdditionalInfo, true);
		OnUpdateWeaponSlot.Broadcast(IndexSlot, NewWeapon);

		result = true;
	}

	return result;
}

bool UTDS_UnrealInventorySystem::GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem &DropItemInfo)
{
	bool result = false;
	FName DropItemName = GetWeaponNameBySlotIndex(IndexSlot);

	UTDS_UnrealGameInstance* myGI = Cast<UTDS_UnrealGameInstance>(GetWorld()->GetGameInstance());
	if (myGI) {
		result = myGI->GetDropItemInfoByWeaponName(DropItemName, DropItemInfo);

		if (WeaponSlots.IsValidIndex(IndexSlot)) {
			DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
		}
	}

	return result;
}

bool UTDS_UnrealInventorySystem::TryGetWeaponToInventory(FWeaponSlot NewWeapon)
{
	int32 IndexSlot = -1;

	if (CheckCanTakeWeapon(IndexSlot)) {
		if (WeaponSlots.IsValidIndex(IndexSlot)) {
			WeaponSlots[IndexSlot] = NewWeapon;
			OnUpdateWeaponSlot.Broadcast(IndexSlot, NewWeapon);
			return true;
		}
	}

	return false;
}

void UTDS_UnrealInventorySystem::DropWeaponByIndex(int32 ByIndex, FDropItem& DropItemInfo)
{
	bool bIsCanDrop = false;
	int8 i = 0;
	int8 AvailableWeaponNum = 0;

	while (i < WeaponSlots.Num() && !bIsCanDrop)
	{
		if (!WeaponSlots[i].NameItem.IsNone())
		{
			AvailableWeaponNum++;

			if (AvailableWeaponNum > 1)
			{
				bIsCanDrop = true;
			}
		}
		i++;
	}

	if (bIsCanDrop && WeaponSlots.IsValidIndex(ByIndex) && GetDropItemInfoFromInventory(ByIndex, DropItemInfo))
	{
		GetDropItemInfoFromInventory(ByIndex,DropItemInfo);

		bool bIsFindWeapon = false;
		int8 j = 0;

		while (j < WeaponSlots.Num() && !bIsFindWeapon)
		{
			if (!WeaponSlots[j].NameItem.IsNone())
			{
				OnSwitchWeapon.Broadcast(WeaponSlots[j].NameItem, WeaponSlots[j].AdditionalInfo, j);
				bIsFindWeapon = true;
			}
			j++;
		}

		const FWeaponSlot EmptyWeaponSlot;
		WeaponSlots[ByIndex] = EmptyWeaponSlot;

		if(GetOwner()->GetClass()->ImplementsInterface(UTDS_UnrealIGameActor::StaticClass()))
		{
			ITDS_UnrealIGameActor::Execute_DropWeaponToWorld(GetOwner(), DropItemInfo);
		}

		OnUpdateWeaponSlot.Broadcast(ByIndex,EmptyWeaponSlot);
	}
}

TArray<FWeaponSlot> UTDS_UnrealInventorySystem::GetWeaponSlots()
{
	return WeaponSlots;
}

TArray<FAmmoSlot> UTDS_UnrealInventorySystem::GetAmmoSlots()
{
	return AmmoSlots;
}

void UTDS_UnrealInventorySystem::InitInventory(TArray<FWeaponSlot> NewWeaponSlots, TArray<FAmmoSlot> NewAmmoSlots)
{
	WeaponSlots = NewWeaponSlots;
	AmmoSlots = NewAmmoSlots;

	MaxWeaponSlots = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0)) {
		if (!WeaponSlots[0].NameItem.IsNone()) {
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
		}
	}
}

