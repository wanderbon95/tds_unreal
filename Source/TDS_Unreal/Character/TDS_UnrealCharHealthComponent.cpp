// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_Unreal/Character/TDS_UnrealCharHealthComponent.h"

#include "TDS_UnrealCharacter.h"
#include "Kismet/GameplayStatics.h"

void UTDS_UnrealCharHealthComponent::CreateShieldEffect()
{
	ATDS_UnrealCharacter* myCH = Cast<ATDS_UnrealCharacter>(GetOwner());
		
	if(myCH)
	{
		ShieldParticleComponent = UGameplayStatics::SpawnEmitterAttached(
			RepairShieldDoneParticleSystem,  
			myCH->GetMesh(), 
			FName("ShieldSocketHead"), 
			FVector(0,0,0), 
			FRotator(0,0,0), 
			EAttachLocation::KeepRelativeOffset, 
			false
		);
	}
}

void UTDS_UnrealCharHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	
	if(GetOwner() && RepairShieldDoneParticleSystem && Shield > 0.0f)
	{
		CreateShieldEffect();
	}
}

void UTDS_UnrealCharHealthComponent::ChangeHealthValue(float ChangeValue)
{
	const float CurrentDamage = ChangeValue * CoefDamage;

	if(Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(CurrentDamage);

		if(ShieldImpactParticleSystem && GetOwner())
		{
			float X = FMath::RandRange(-100.0f, 100.0f);
			FVector ImpactLocation = FVector(X, 50, 50);

			ImpactLocation = ImpactLocation + GetOwner()->GetActorLocation();
			
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ShieldImpactParticleSystem, ImpactLocation);
		}
		
		if(Shield <= 0.0f)
		{
			if(CrashShieldParticleSystem)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), CrashShieldParticleSystem, GetOwner()->GetActorLocation());
			}

			if(ShieldParticleComponent)
			{
				ShieldParticleComponent->SetVisibility(false);
			}
		}
	} else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UTDS_UnrealCharHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTDS_UnrealCharHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;
	
	if(Shield > 100.0f)
	{
		Shield = 100.0f;
	} else
	{
		if(Shield <= 0.0f)
		{
			Shield = 0.0f;
		}
	}

	OnShieldChange.Broadcast(Shield, abs(ChangeValue));

	if(GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UTDS_UnrealCharHealthComponent::CollDownShieldEnd, CollDownShieldRepairTime, false);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRepairRateTimer);
	}
}

void UTDS_UnrealCharHealthComponent::CollDownShieldEnd()
{
	if(GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRepairRateTimer, this, &UTDS_UnrealCharHealthComponent::RepairShield, ShieldRepairRate, true);
	}
}

void UTDS_UnrealCharHealthComponent::RepairShield()
{
	float tmp = Shield;
	tmp += ShieldRepairValue;

	if(tmp > 100.0f)
	{
		Shield = 100.0f;
		
		if(GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRepairRateTimer);
		}
	} else
	{
		Shield = tmp;
	}

	if(Shield > 0.0f && RepairShieldDoneParticleSystem)
	{	
		if(ShieldParticleComponent)
		{
			ShieldParticleComponent->SetVisibility(true);
		} else
		{
			CreateShieldEffect();
		}
	}

	OnShieldChange.Broadcast(Shield, ShieldRepairRate);
}
