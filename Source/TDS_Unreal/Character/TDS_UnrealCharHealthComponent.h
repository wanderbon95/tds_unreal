// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_Unreal/Character/TDS_UnrealHealthComponent.h"
#include "TDS_UnrealCharHealthComponent.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS()
class TDS_UNREAL_API UTDS_UnrealCharHealthComponent : public UTDS_UnrealHealthComponent
{
	GENERATED_BODY()

	UParticleSystemComponent* ShieldParticleComponent = nullptr;
	void CreateShieldEffect();
public:
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category="Health")
		FOnShieldChange OnShieldChange;
	
	FTimerHandle TimerHandle_CollDownShieldTimer;
	FTimerHandle TimerHandle_ShieldRepairRateTimer;
protected:
	float Shield = 100.0f;

	virtual void BeginPlay() override;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CollDownShieldRepairTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRepairValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRepairRate = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		UParticleSystem* CrashShieldParticleSystem = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		UParticleSystem* RepairShieldDoneParticleSystem = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		UParticleSystem* ShieldImpactParticleSystem = nullptr;
	
	virtual void ChangeHealthValue(float ChangeValue) override;
	float GetCurrentShield();
	void ChangeShieldValue(float ChangeValue);

	void CollDownShieldEnd();
	void RepairShield();
};