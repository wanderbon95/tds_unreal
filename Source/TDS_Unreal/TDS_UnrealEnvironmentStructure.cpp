// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_UnrealEnvironmentStructure.h"

#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
ATDS_UnrealEnvironmentStructure::ATDS_UnrealEnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATDS_UnrealEnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDS_UnrealEnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATDS_UnrealEnvironmentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));

	if(myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);

		if(myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}

	return Result;
}

TArray<UTDS_UnrealStateEffect*> ATDS_UnrealEnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void ATDS_UnrealEnvironmentStructure::RemoveEffect(UTDS_UnrealStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATDS_UnrealEnvironmentStructure::AddEffect(UTDS_UnrealStateEffect* AddEffect)
{
	Effects.Add(AddEffect);
}

FVector ATDS_UnrealEnvironmentStructure::GetEffectSettings(FName& BoneName)
{
	BoneName = FName();

	return EffectOffset;
}

