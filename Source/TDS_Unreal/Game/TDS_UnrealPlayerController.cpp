// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_UnrealPlayerController.h"
#include "../Character/TDS_UnrealCharacter.h"

ATDS_UnrealPlayerController::ATDS_UnrealPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ATDS_UnrealPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
}

void ATDS_UnrealPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
}

void ATDS_UnrealPlayerController::OnUnPossess()
{
	Super::OnUnPossess();
}
