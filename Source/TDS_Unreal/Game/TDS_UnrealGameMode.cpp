// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_UnrealGameMode.h"
#include "TDS_UnrealPlayerController.h"
#include "UObject/ConstructorHelpers.h"
#include "TDS_Unreal/Character/TDS_UnrealCharacter.h"

ATDS_UnrealGameMode::ATDS_UnrealGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS_UnrealPlayerController::StaticClass();
	// set default pawn class to our Blueprinted character
	// static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Characters/PlayerCharacter/BP_Character"));
	
	// if (PlayerPawnBPClass.Succeeded())
	// {
		// PlayerPawnBPClass.Class->AddToRoot();
		// DefaultPawnClass = PlayerPawnBPClass.Class;
	// }
}

void ATDS_UnrealGameMode::PlayerCharacterDead()
{
}
