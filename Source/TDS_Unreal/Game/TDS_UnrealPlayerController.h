// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TDS_UnrealPlayerController.generated.h"

UCLASS()
class ATDS_UnrealPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATDS_UnrealPlayerController();
protected:
	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	virtual void OnUnPossess() override;
};


