#pragma once

#include "CoreMinimal.h"

#include "TDS_Unreal/FuncLibrary/Types.h"
#include "UObject/Interface.h"
#include "TDS_Unreal/StateEffects/TDS_UnrealStateEffect.h"
#include "TDS_UnrealIGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDS_UnrealIGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_UNREAL_API ITDS_UnrealIGameActor
{
	GENERATED_BODY()
public:
	virtual EPhysicalSurface GetSurfaceType();
	virtual TArray<UTDS_UnrealStateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UTDS_UnrealStateEffect* RemoveEffect);
	virtual void AddEffect(UTDS_UnrealStateEffect* AddEffect);
	virtual FVector GetEffectSettings(FName& BoneName);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void DropWeaponToWorld(FDropItem DropItemInfo);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Cout);
};
