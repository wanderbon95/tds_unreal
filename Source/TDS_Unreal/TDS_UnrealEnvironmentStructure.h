// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/TDS_UnrealIGameActor.h"
#include "StateEffects/TDS_UnrealStateEffect.h"

#include "TDS_UnrealEnvironmentStructure.generated.h"

UCLASS(Blueprintable)
class TDS_UNREAL_API ATDS_UnrealEnvironmentStructure : public AActor, public ITDS_UnrealIGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDS_UnrealEnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual EPhysicalSurface GetSurfaceType() override;

	TArray<UTDS_UnrealStateEffect*> Effects;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect Spawn Point Setting")
		FVector EffectOffset = FVector(0);
	
	virtual TArray<UTDS_UnrealStateEffect*> GetAllCurrentEffects() override;
	virtual void RemoveEffect(UTDS_UnrealStateEffect* RemoveEffect) override;
	virtual void AddEffect(UTDS_UnrealStateEffect* AddEffect) override;
	virtual FVector GetEffectSettings(FName& BoneName) override;
};
