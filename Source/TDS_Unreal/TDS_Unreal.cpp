// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_Unreal.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_Unreal, "TDS_Unreal" );

DEFINE_LOG_CATEGORY(LogTDS_Unreal)
 