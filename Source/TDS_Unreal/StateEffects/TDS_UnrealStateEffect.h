// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "UObject/NoExportTypes.h"
#include "TDS_UnrealStateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TDS_UNREAL_API UTDS_UnrealStateEffect : public UObject
{
	GENERATED_BODY()
	EPhysicalSurface CurrentSurfaceType = EPhysicalSurface::SurfaceType_Default;
public:
	FName BoneName = NAME_None;
	AActor* MyActor = nullptr;
	
	virtual bool InitObject(AActor* Actor, FName BoneHitName = NAME_None);
	virtual void DestroyObject();
	virtual void ApplyVisual();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Setting")
		TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Setting")
		bool bIsStackable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		TMap<TEnumAsByte<EPhysicalSurface>, USoundBase*> HitSounds;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> ImpactFxs;
};

UCLASS()
class TDS_UNREAL_API UTDS_UnrealStateEffect_ExecuteOnce : public UTDS_UnrealStateEffect
{
	GENERATED_BODY()
public:
	virtual bool InitObject(AActor* Actor, FName BoneHitName = NAME_None) override;
	virtual void DestroyObject() override;
	virtual void ApplyVisual() override;
	
	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Setting Execute Once")
		float Power = 20.0f;
};

UCLASS()
class TDS_UNREAL_API UTDS_UnrealStateEffect_ExecuteTimer : public UTDS_UnrealStateEffect
{
	GENERATED_BODY()
public:
	virtual bool InitObject(AActor* Actor, FName BoneHitName = NAME_None) override;
	virtual void DestroyObject() override;
	virtual void ApplyVisual() override;
	
	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Setting Execute Once")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Setting Execute Once")
		float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Setting Execute Once")
		float RateTime = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Setting Execute Once")
		UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;
};
