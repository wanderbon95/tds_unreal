// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_Unreal/StateEffects/TDS_UnrealStateEffect.h"

#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "TDS_Unreal/Character/TDS_UnrealCharacter.h"
#include "TDS_Unreal/Character/TDS_UnrealHealthComponent.h"
#include "TDS_Unreal/Interface/TDS_UnrealIGameActor.h"

bool UTDS_UnrealStateEffect::InitObject(AActor* Actor, FName BoneHitName)
{
	MyActor = Actor;
	BoneName = BoneHitName;
	
	if (MyActor)
	{
		ITDS_UnrealIGameActor* MyInterface = Cast<ITDS_UnrealIGameActor>(MyActor);

		if (MyInterface)
		{
			CurrentSurfaceType = MyInterface->GetSurfaceType();
			MyInterface->AddEffect(this);
		}

		ATDS_UnrealCharacter* MyCharacter = Cast<ATDS_UnrealCharacter>(MyActor);

		if (MyCharacter && MyCharacter->bIsAbility)
		{
			MyCharacter->bIsAbility = false;
		}
	}
	
	return true;
}

void UTDS_UnrealStateEffect::DestroyObject()
{
	ITDS_UnrealIGameActor* MyInterface = Cast<ITDS_UnrealIGameActor>(MyActor);

	if (MyInterface)
	{
		MyInterface->RemoveEffect(this);
	}
	
	MyActor = nullptr;
	if (this && this->IsValidLowLevelFast())
	{
		this->ConditionalBeginDestroy();
	}
}

void UTDS_UnrealStateEffect::ApplyVisual()
{
	ITDS_UnrealIGameActor* MyInterface = Cast<ITDS_UnrealIGameActor>(MyActor);
	
	if (MyInterface)
	{
		FName AttachedBoneName;
		const FVector EffectLocation = MyInterface->GetEffectSettings(AttachedBoneName);

		if (!AttachedBoneName.IsNone())
		{
			BoneName = AttachedBoneName;
		} 

		if (CurrentSurfaceType)
		{
			if (ImpactFxs.Contains(CurrentSurfaceType))
			{
				UParticleSystem* MyParticle = ImpactFxs[CurrentSurfaceType];
				
				if (MyParticle)
				{
					USceneComponent* MySkeletalMesh = Cast<USceneComponent>(MyActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
					USceneComponent* ComponentForAttach = MyActor->GetRootComponent();
					
					if (MySkeletalMesh)
					{
						ComponentForAttach = MySkeletalMesh;
					}

					UGameplayStatics::SpawnEmitterAttached(
							MyParticle,
							ComponentForAttach,
							BoneName,
							EffectLocation,
							FRotator::ZeroRotator,
							EAttachLocation::SnapToTarget,
							false
						);
				}
			}

			if (HitSounds.Contains(CurrentSurfaceType))
			{	
				USoundBase* MySound = HitSounds[CurrentSurfaceType];

				if (MySound) {
					UGameplayStatics::PlaySoundAtLocation(GetWorld(), MySound, MyActor->GetActorLocation());;
				}
			}		
		}
	}
}

bool UTDS_UnrealStateEffect_ExecuteOnce::InitObject(AActor* Actor, FName BoneHitName)
{
	Super::InitObject(Actor, BoneHitName);
	BoneName = BoneHitName;
	ExecuteOnce();
	
	return true;
}

void UTDS_UnrealStateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_UnrealStateEffect_ExecuteOnce::ApplyVisual()
{
	Super::ApplyVisual();
}

void UTDS_UnrealStateEffect_ExecuteOnce::ExecuteOnce()
{
	ATDS_UnrealCharacter* MyCharacter = Cast<ATDS_UnrealCharacter>(MyActor);
	
	if (MyCharacter && MyCharacter->bIsAlive)
	{
		ApplyVisual();
		
		UTDS_UnrealHealthComponent* MyHealthComponent = Cast<UTDS_UnrealHealthComponent>(MyActor->GetComponentByClass(UTDS_UnrealHealthComponent::StaticClass()));		

		if (MyHealthComponent)
		{
			MyHealthComponent->ChangeHealthValue(Power);
		}
	}

	DestroyObject();
}

bool UTDS_UnrealStateEffect_ExecuteTimer::InitObject(AActor* Actor, FName BoneHitName)
{
	Super::InitObject(Actor, BoneHitName);
	BoneName = BoneHitName;
	
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_UnrealStateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_UnrealStateEffect_ExecuteTimer::Execute, RateTime, true);
	
	if (ParticleEffect)
	{
		ITDS_UnrealIGameActor* MyInterface = Cast<ITDS_UnrealIGameActor>(Actor);

		if (MyInterface)
		{
			FName AttachedBoneName;
			const FVector EffectLocation = MyInterface->GetEffectSettings(AttachedBoneName);
			
			if (!AttachedBoneName.IsNone())
			{
				BoneName = AttachedBoneName;
			}

			USceneComponent* MySkeletalMesh = Cast<USceneComponent>(MyActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
			USceneComponent* ComponentForAttach = MyActor->GetRootComponent();
					
			if (MySkeletalMesh)
			{
				ComponentForAttach = MySkeletalMesh;
			}
		
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(
				ParticleEffect,
				ComponentForAttach,
				BoneName,
				EffectLocation,
				FRotator::ZeroRotator,
				EAttachLocation::SnapToTarget,
				false
			);
		}
	}
	
	return true;
}

void UTDS_UnrealStateEffect_ExecuteTimer::DestroyObject()
{
	if (ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}
	
	Super::DestroyObject();
}

void UTDS_UnrealStateEffect_ExecuteTimer::ApplyVisual()
{
	Super::ApplyVisual();
}

void UTDS_UnrealStateEffect_ExecuteTimer::Execute()
{
	if (MyActor)
	{
		ATDS_UnrealCharacter* MyCharacter = Cast<ATDS_UnrealCharacter>(MyActor);

		if(!MyCharacter || (MyCharacter && MyCharacter->bIsAlive))
		{
			ApplyVisual();
			
			UTDS_UnrealHealthComponent* MyHealthComponent = Cast<UTDS_UnrealHealthComponent>(MyActor->GetComponentByClass(UTDS_UnrealHealthComponent::StaticClass()));		

			if (MyHealthComponent)
			{
				MyHealthComponent->ChangeHealthValue(Power);
			}
		} else if(MyCharacter && !MyCharacter->bIsAlive)
		{
			DestroyObject();
		}
	}
}
